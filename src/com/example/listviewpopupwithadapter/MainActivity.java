package com.example.listviewpopupwithadapter;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.app.Activity;
import android.app.AlertDialog;

public class MainActivity extends Activity {

	AlertDialog alertDialogStores;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        // a button to show the pop up with a list view
        View.OnClickListener handler = new View.OnClickListener(){
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.buttonShowPopUp:
                    	showPopUp();
                        break;
                }
            }
        };
        
        findViewById(R.id.buttonShowPopUp).setOnClickListener(handler);
        
    }
    
    public void showPopUp(){
    	
    	// add your items, this can be done programatically
    	// your items can be from a database
        ObjectItem[] ObjectItemData = new ObjectItem[40];
        
        ObjectItemData[0] = new ObjectItem(91, "Mercury");
        ObjectItemData[1] = new ObjectItem(92, "Watson");
        ObjectItemData[2] = new ObjectItem(93, "Nissan");
        ObjectItemData[3] = new ObjectItem(94, "Puregold");
        ObjectItemData[4] = new ObjectItem(95, "SM");
        ObjectItemData[5] = new ObjectItem(96, "7 Eleven");
        ObjectItemData[6] = new ObjectItem(97, "Ministop");
        ObjectItemData[7] = new ObjectItem(98, "Fat Chicken");
        ObjectItemData[8] = new ObjectItem(99, "Master Siomai");
        ObjectItemData[9] = new ObjectItem(100, "Mang Inasal");
        ObjectItemData[10] = new ObjectItem(101, "Mercury 2");
        ObjectItemData[11] = new ObjectItem(102, "Watson 2");
        ObjectItemData[12] = new ObjectItem(103, "Nissan 2");
        ObjectItemData[13] = new ObjectItem(104, "Puregold 2");
        ObjectItemData[14] = new ObjectItem(105, "SM 2");
        ObjectItemData[15] = new ObjectItem(106, "7 Eleven 2");
        ObjectItemData[16] = new ObjectItem(107, "Ministop 2");
        ObjectItemData[17] = new ObjectItem(108, "Fat Chicken 2");
        ObjectItemData[18] = new ObjectItem(109, "Master Siomai 2");
        ObjectItemData[19] = new ObjectItem(110, "Mang Inasal 2");
        
        ObjectItemData[20] = new ObjectItem(111, "Mercury 3");
        ObjectItemData[21] = new ObjectItem(112, "Watson 3");
        ObjectItemData[22] = new ObjectItem(113, "Nissan 3");
        ObjectItemData[23] = new ObjectItem(114, "Puregold 3");
        ObjectItemData[24] = new ObjectItem(115, "SM 3");
        ObjectItemData[25] = new ObjectItem(116, "7 Eleven 3");
        ObjectItemData[26] = new ObjectItem(117, "Ministop 3");
        ObjectItemData[27] = new ObjectItem(118, "Fat Chicken 3");
        ObjectItemData[28] = new ObjectItem(119, "Master Siomai 3");
        ObjectItemData[29] = new ObjectItem(120, "Mang Inasal 3");
        ObjectItemData[30] = new ObjectItem(121, "Mercury 4");
        ObjectItemData[31] = new ObjectItem(122, "Watson 4");
        ObjectItemData[32] = new ObjectItem(123, "Nissan 4");
        ObjectItemData[33] = new ObjectItem(124, "Puregold 4");
        ObjectItemData[34] = new ObjectItem(125, "SM 4");
        ObjectItemData[35] = new ObjectItem(126, "7 Eleven 4");
        ObjectItemData[36] = new ObjectItem(127, "Ministop 4");
        ObjectItemData[37] = new ObjectItem(128, "Fat Chicken 4");
        ObjectItemData[38] = new ObjectItem(129, "Master Siomai 4");
        ObjectItemData[39] = new ObjectItem(130, "Mang Inasal 4");
        
        // our adapter instance
        ArrayAdapterItem adapter = new ArrayAdapterItem(this, R.layout.list_view_row_item, ObjectItemData);
        
        // create a new ListView, set the adapter and item click listener
        ListView listViewItems = new ListView(this);
        listViewItems.setAdapter(adapter);
        listViewItems.setOnItemClickListener(new OnItemClickListenerListViewItem());
        
        // put the ListView in the pop up
        alertDialogStores = new AlertDialog.Builder(MainActivity.this)
	        .setView(listViewItems)
	        .setTitle("Stores")
	        .show();
        
    }

    
}
